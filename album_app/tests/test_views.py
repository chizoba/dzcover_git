from django.test import TestCase, Client, mock, RequestFactory
from album_app.models import *
from album_app.forms import *
from django.core.urlresolvers import reverse
from django.core.files.uploadedfile import SimpleUploadedFile
from PIL import Image
import tempfile
from django.test import override_settings


# import mock
def get_temporary_image(temp_file):
    size = (200, 200)
    color = (255, 0, 0, 0)
    image = Image.new("RGBA", size, color)
    image.save(temp_file, 'jpeg')
    return temp_file

class AlbumAppViewsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.samuel = Profile.objects.create_user(
        first_name = 'Samuel',
        email = 'sam@yahoo.com', username='samdon',
        password = 'sam1234'
        )
        cls.samuel.is_active = True
        cls.samuel.save()
        cls.john = Profile.objects.create_user(
        first_name = 'John',
        email = 'john@yahoo.com', username='John',
        password = 'john1234'
        )
        cls.john.is_active = True
        cls.john.save()
        cls.client =  Client()

        cls.client = Client()
        # cls.request = RequestFactory()
    # @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    # def test_dummy_test(self):
    #     temp_file = tempfile.NamedTemporaryFile()
    #     test_image = get_temporary_image(temp_file)
    #     #test_image.seek(0)
    #     picture = Album.objects.create(photo=test_image.name)
    #     print("It Worked!,")
    #     self.assertEqual(len(Album.objects.all()), 1)
    def test_upload_form(self):
        temp_file = tempfile.NamedTemporaryFile()
        test_image = get_temporary_image(temp_file)
        test_image .seek(0)
        # picture = Album.objects.create(photo=test_image.name)
        # print("It Worked!,")
        # self.assertEqual(len(Album.objects.all()), 1)
        user = Profile.objects.get(username = self.samuel.username )
        # photo= SimpleUploadedFile('image.jpeg', content_type="image/jpeg")
        data = {
        'caption' : 'hey',
        }
        file_data = {
            'photo': test_image,
        }
        form = UploadForm(data, file_data)
        self.assertTrue(form.is_valid())
        # self.request = RequestFactory
        # album = form.save(commit = False)
        # album.user = user
        # album.save()
        # self.assertTrue(form.is_valid())
        # self.assertEqual(album.content, "hey")
    # self.assertEqual(comment.email, "leela@example.com")
    # self.assertEqual(comment.body, "Hi there")
    # self.assertEqual(comment.entry, self.entry)


    def test_view_album(self):
        #for initially getting album
        self.client.login(email = 'sam@yahoo.com', password= 'sam1234')
        response = self.client.get(reverse('album:view_album', kwargs={'username': self.samuel.username}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'album/view_album.html')
        self.assertTrue('view_user' and 'album' and 'form' and 'total' and 'custom_page_range' in response.context )
        #for posting
        # photo= SimpleUploadedFile('image.txt',  b"file_content")
        # response = self.client.post(reverse('album:view_album', kwargs={'username': self.samuel.username}), {'photo':photo, 'caption': 'hey'})
        # self.assertEqual(response.status_code, 302)
    def test_delete_photo(self):
        #profile that does not exists
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user = Profile.objects.get(username = self.samuel.username )
        album = Album.objects.create(photo= SimpleUploadedFile('image.txt',  b"content"), user=user)
        response = self.client.get(reverse('album:delete_photo', kwargs={'username': 'jony'}), {'photo-id': album.id})
        self.assertEqual(response.status_code, 404)

        #image that does not exists
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user = Profile.objects.get(username = self.samuel.username )
        album2 = Album.objects.create(photo= SimpleUploadedFile('image.txt',  b"content"), user=user)
        response = self.client.get(reverse('album:delete_photo', kwargs={'username': self.samuel.username}), {'photo-id': 5})
        self.assertEqual(response.status_code, 404)
        #image user and profile user dont match
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user = Profile.objects.get(username = self.john.username )
        album3 = Album.objects.create(photo= SimpleUploadedFile('image.txt',  b"content"), user=user)
        response = self.client.get(reverse('album:delete_photo', kwargs={'username': self.john.username}), {'photo-id': album3.id})
        self.assertEqual(response.status_code, 404)
        #succesful delete
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user = Profile.objects.get(username = self.samuel.username )
        album4 = Album.objects.create(photo= SimpleUploadedFile('image.txt',  b"content"), user=user)
        response = self.client.get(reverse('album:delete_photo', kwargs={'username': self.samuel.username}), {'photo-id': album4.id})
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'result': 'success', 'total_photos': 2}
        )
