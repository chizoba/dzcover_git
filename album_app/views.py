from __future__ import division
from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from .forms import UploadForm
from django.http import HttpResponse, Http404, JsonResponse
from django.views import generic
from django.views.generic import View, ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from user_app.models import Profile
from .models import Album
from django.core.urlresolvers import reverse
from PIL import Image
from django.core.files import File
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json


# Create your views here.

class ViewAlbum(LoginRequiredMixin, View):
    login_url ='/'
    def get(self, request, username):
        user = get_object_or_404(Profile, username = username)
        photos_list = Album.objects.filter(user = user).order_by('-created_on')
        paginator = Paginator(photos_list, 10)
        page = request.GET.get('page')
        try:
            album = paginator.page(page)
        except PageNotAnInteger:
            album = paginator.page(1)
        except EmptyPage:
            album = paginator.page(paginator.num_pages)
        total = photos_list.count()
        form = UploadForm()
        return render(request, 'album/view_album.html', {'view_user': user, 'album': album, 'form': form,
        'total': total})
    def post(self, request, username):
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            upload = form.save(commit = False)
            upload.user = request.user
            upload.save()
            image = Image.open(upload.photo)
            width,height = image.size
            resize_width = 600
            resize_height = 600
            if width > height :
                height = height * (resize_width/width)
                width = resize_width
            else:
                width = width * (resize_height/height)
                height = resize_height
            resized_image = image.resize(( int(width),int(height)), Image.ANTIALIAS)
            resized_image.convert('RGB').save(upload.photo.path, format = 'JPEG')
            upload.save()
        else:
            return HttpResponse(form.errors)
        return redirect(reverse('album:view_album', kwargs = {'username':request.user.username}))

@login_required(login_url ='/')
def delete_photo(request,username):
    user = get_object_or_404(Profile, username = username)
    photo = get_object_or_404(Album, id = request.GET.get('photo-id'))
    if photo.user != request.user :
        raise Http404
    else:
        photo.delete()
        count = user.album.count()
        data = {'result': 'success', 'total_photos': count}
        return HttpResponse(json.dumps(data), content_type ='application/json')
