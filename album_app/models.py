from django.db import models
from user_app.models import Profile
from django.utils import timezone
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
# Create your models here.
class Album(models.Model):
    user = models.ForeignKey(Profile, related_name='album')
    photo = models.ImageField(upload_to='gallery')
    caption = models.CharField(max_length=130, blank = True, null= True)
    created_on = models.DateTimeField(default = timezone.now)
    class Meta:
        ordering=['-created_on']

    def __str__(self):
        return '{} posted a picture'.format(self.user)


# Receive the pre_delete signal and delete the file associated with the model instance.
@receiver(pre_delete, sender=Album)
def mymodel_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    if instance.photo:
        instance.photo.delete(False)
