from django.conf.urls import url,include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    url(r'^$', views.ViewAlbum.as_view(), name= 'view_album'),
    url(r'^delete/$', views.delete_photo, name='delete_photo'),
]
