from django.forms import ModelForm
from .models import Album
from django import forms
from django.db import models
from django.shortcuts import get_object_or_404
from PIL import Image
from django.core.files import File

class UploadForm(ModelForm):
    photo = forms.ImageField()
    caption = forms.CharField(max_length= 130, widget=forms.TextInput(attrs={'placeholder':'add a caption','class':'form-control'}), label='', required=False)
    class Meta:
        model = Album
        fields = ('photo', 'caption')
