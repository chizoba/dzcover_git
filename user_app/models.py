from django.db import models
from django.contrib import auth
from django.core.exceptions import PermissionDenied
from django.contrib.auth.models import User, AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.contrib.sites.models import Site
from django.contrib.postgres.search import SearchVectorField, SearchVector
from django.db.models.signals import post_save
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver
from itertools import chain
from operator import attrgetter
import datetime
import pytz
import job_app
import uuid
from django.utils.http import urlquote
import warnings
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class MyProfileManager(BaseUserManager):
    def create_user(self, email, first_name, username, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            username = username,
        )

        user.set_password(password)
        user.is_active = True;
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, username, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
            first_name=first_name,
            username=username
        )
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def with_documents(self):
        vector = SearchVector('first_name', weight='A') + \
                 SearchVector('last_name', weight='B') + \
                 SearchVector('profession', weight='A') + \
                 SearchVector('location', weight='C') +\
                 SearchVector('about_me', weight='C')

        return self.get_queryset().annotate(document=vector)


class Profile(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(max_length=35)
    last_name = models.CharField(max_length=35)
    email = models.EmailField(
            verbose_name='email address',
            max_length=255,
            unique=True,)
    password= models.CharField(max_length=100)
    username = models.CharField(unique=True, max_length=20)
    location = models.CharField(max_length=150, blank = True, null= True)
    image = models.ImageField(upload_to='profile_images', blank=True, null= True)
    profession = models.CharField(max_length=150, blank =True, null =True)
    current_job = models.CharField(max_length=150, blank=True, null= True)
    introduction = models.TextField(max_length= 150 , blank = True, null = True)
    about_me = models.TextField(max_length= 5000, blank = True, null = True)
    website = models.URLField(blank=True, null= True)
    facebook_link = models.URLField(blank=True, null= True)
    twitter_link = models.URLField(blank=True, null= True)
    youtube_link = models.URLField(blank=True, null= True)
    date_joined = models.DateTimeField(default=timezone.now)
    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_superuser  = models.BooleanField(default=False)
    search_vector = SearchVectorField(null=True, blank = True)
    delete_account = models.BooleanField(default=False)


    objects = MyProfileManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name','username']
    class Meta:
        ordering= ['first_name']
    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)
        if 'update_fields' not in kwargs or 'search_vector' not in kwargs['update_fields']:
            instance = self._meta.default_manager.with_documents().get(pk=self.pk)
            instance.search_vector = instance.document
            instance.save(update_fields=['search_vector'])

    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def get_absolute_url(self):
        return reverse('view_user', kwargs= {'username' : self.username})

    def get_full_absolute_url(self):
        domain=Site.objects.get_current().domain
        return '%s%s' % (domain, self.get_absolute_url())

    def get_short_name(self):
        return self.first_name

    def get_header(self):
        if len(str(self.get_full_name()))<=27:
            return self.get_full_name().title()
        else:
            return self.get_initials().upper()
    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def get_avatar(self):
        if self.image:
            return self.image.url
        else:
            return r'/static/images/profile/default-avatar.png'
    def get_location(self):
        if self.location:
            return self.location
        else :
            return '-'
    def get_profession(self):
        if self.profession:
	    return self.profession
	else:
	    return '' 
    def get_current_job(self):
        if self.current_job:
            return self.current_job
        else :
            return '-'
    def get_introduction(self):
        if self.introduction:
            return self.introduction
        else :
            return "Hello I'm {}".format(self.first_name)
    def get_about_me(self):
        if self.about_me:
            return self.about_me
        else :
            return "-"


    def get_website(self):
        if self.website:
            return self.website
        else:
            return 'none'
    def get_twitter(self):
        if self.twitter_link:
            return self.twitter_link
        else:
            return 'none'
    def get_youtube(self):
        if self.youtube_link:
            return self.youtube_link
        else:
            return 'none'
    def get_facebook(self):
        if self.facebook_link:
            return self.facebook_link
        else:
            return 'none'
    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])
    def get_initials(self):
        return '{}{}'.format(self.first_name[0], self.last_name[0])

    def count_network(self):
        total = self.network.count()
        total_str = str(total)
        if total >= 10000000:
            return '{}.{}B'.format(total_str[:2], total_str[2])
        if total >= 1000000000:
            return '{}.{}B'.format(total_str[0], total_str[1])
        if total >= 100000000:
            return '{}M'.format(total_str[:3])
        if total >= 10000000:
            return '{}.{}M'.format(total_str[:2], total_str[2])
        if total >= 1000000:
            return '{}.{}M'.format(total_str[0], total_str[1])
        if total >= 100000:
            return '{}K'.format(total_str[:3])
        if total >= 10000:
            return '{}.{}K'.format(total_str[:2], total_str[2])
        else:
            return total

    def count_recommended(self):
        total = self.recommended.count()
        total_str = str(total)
        if total >= 10000000:
            return '{}.{}B'.format(total_str[:2], total_str[2])
        if total >= 1000000000:
            return '{}.{}B'.format(total_str[0], total_str[1])
        if total >= 100000000:
            return '{}M'.format(total_str[:3])
        if total >= 10000000:
            return '{}.{}M'.format(total_str[:2], total_str[2])
        if total >= 1000000:
            return '{}.{}M'.format(total_str[0], total_str[1])
        if total >= 100000:
            return '{}K'.format(total_str[:3])
        if total >= 10000:
            return '{}.{}K'.format(total_str[:2], total_str[2])
        else:
            return total

    def count_notifications(self):
        total = self.notifications.filter(unread= True).count()
        total_str = str(total)
        if total >= 10000000:
            return '{}.{}B'.format(total_str[:2], total_str[2])
        if total >= 1000000000:
            return '{}.{}B'.format(total_str[0], total_str[1])
        if total >= 100000000:
            return '{}M'.format(total_str[:3])
        if total >= 10000000:
            return '{}.{}M'.format(total_str[:2], total_str[2])
        if total >= 1000000:
            return '{}.{}M'.format(total_str[0], total_str[1])
        if total >= 100000:
            return '{}K'.format(total_str[:3])
        if total >= 10000:
            return '{}.{}K'.format(total_str[:2], total_str[2])
        else:
            return total


class Notification(models.Model):
    from_user = models.ForeignKey(Profile, related_name ='sent_notifications')
    to_user = models.ForeignKey(Profile, related_name='notifications')
    message = models.TextField()
    category = models.CharField(max_length = 60)
    created_on = models.DateTimeField(default = timezone.now)
    unread = models.BooleanField(default = True)

    def __str__(self):
        return 'Notification for {}'.format(self.to_user)

    class Meta:
        ordering =['-created_on']

class UserRecommendation(models.Model):
    from_user = models.ForeignKey(Profile, related_name='recommends')
    to_user = models.ForeignKey(Profile, related_name ='recommended')
    created_on = models.DateTimeField(default = timezone.now)

    class Meta:
        unique_together =(('from_user', 'to_user'),)
        ordering = ['to_user']

    def __str__(self):
        return ('{} recommends {}').format(self.from_user, self.to_user)

    def save(self, *args, **kwargs):
        Notification.objects.create(from_user = self.from_user , to_user = self.to_user, message="recommended you", category='UserRecommendation')
        super(UserRecommendation, self).save(*args, **kwargs)

    def get_user_recommends(self, from_user):
        user = from_user
        all_recommendation = [recommendation.to_user for recommendation in user.recommends.all()]
        return all_recommendation

    def get_user_recommended(self, to_user):
        user = to_user
        all_recommends = [recommendation.from_user for recommendation in user.recommended.all()]
        return all_recommends

class NetworkRequest(models.Model):
    from_user = models.ForeignKey(Profile, related_name = 'network_request_sent', null=True, blank=True)
    to_user = models.ForeignKey(Profile, related_name='network_request_received', null=True, blank=True)
    sent_on = models.DateTimeField(default= timezone.now)

    class Meta:
        verbose_name=('Network Request')
        verbose_name_plural = ('Network Requests')
        unique_together = (('from_user', 'to_user'))

    def __str__(self):
       return '{}, {} sent you a request'.format(self.to_user, self.from_user)


    def save(self, *args, **kwargs):
        if self.to_user == self.from_user:
            raise ValidationError('user cannot send request to themselves')
        Notification.objects.create(from_user= self.from_user, to_user= self.to_user, message='sent you a request', category = 'NetworkRequest')
        super(NetworkRequest, self).save(*args, **kwargs)
    #  not in use
    def send_request(self, from_user, to_user):
        request = NetworkRequest.objects.create(from_user = from_user, to_user = to_user)
    def cancel_request(self):
        self.delete()

    def decline_request(self):
        self.delete()

    def accept_request(self):
        network1 = Network.objects.create(from_user=self.from_user, to_user=self.to_user)
        self.delete()
    def count_request_received(self, to_user):
        user = to_user
        return user.network_request_received.all().count()

    def all_request_received(self, to_user):
        user = to_user
        user_request_received = [network_request.from_user for network_request in user.network_request_received.all()]
        return user_request_received

class Network(models.Model):
    from_user = models.ForeignKey(Profile, related_name= 'network', null=True, blank=True)
    to_user = models.ForeignKey(Profile, related_name='other_user', null=True, blank=True)
    created_on = models.DateField(default= timezone.now)

    class Meta:
         verbose_name=('Network')
         verbose_name_plural = ('Network')
         unique_together = (('to_user', 'from_user'),)
         permissions = (('can list network', 'can list network'), )
         ordering = ['from_user']

    def __str__(self):
        return '{} and {}'.format(self.from_user,self.to_user)

    def save(self, *args, **kwargs):
        if self.to_user == self.from_user:
            raise ValidationError('user cannot Network themselves')
        super(Network, self).save(*args, **kwargs)
        try:
            Network.objects.get(from_user= self.to_user, to_user= self.from_user)
            Notification.objects.create(from_user= self.to_user, to_user= self.from_user, message='is now connected with you', category = 'Network')
        except(Network.DoesNotExist):
            Network.objects.create(from_user= self.to_user, to_user= self.from_user)

    def remove_network(self, from_user, to_user):
        from_user = from_user
        to_user = to_user
        from_user.network.get(to_user= to_user).delete()
        to_user.network.get(to_user = from_user).delete()

    def network_count(self, from_user):
        from_user =from_user
        return from_user.network.all().count()

    def all_network(self, from_user):
        from_user = from_user
        from_user_network = [network.to_user for network in from_user.network.all()]
        from_user_network = sorted(from_user_network, key=attrgetter('first_name'))
        return from_user_network

@receiver(pre_save, sender=UserRecommendation)
def mymodel_save(sender, instance, **kwargs):
    if instance.from_user.recommends.count() == 20:
        raise Exception('Reached limit')
