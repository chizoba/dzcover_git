from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import (Profile, Network, NetworkRequest,Notification,
UserRecommendation)

class ProfileCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = Profile
        fields= ('email', 'first_name', 'username')

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('passwords dont match')
        return password2

    def save(self, commit=True):
        user = super().save(commit= False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

class ProfileChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model= Profile
        fields = ('email', 'password','first_name','last_name','username', 'is_active', 'is_admin',
        'profession','location',
        'image','current_job','about_me','website','facebook_link','twitter_link','youtube_link', )
    def clean_password(self):
        return self.initial['password']


 # The forms to add and change user instances
class ProfileAdmin(BaseUserAdmin):
    form = ProfileChangeForm
    add_form = ProfileCreationForm
    list_display = ('email','first_name','username', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password', 'username',)}),
        ('Personal info', {'fields': ('first_name','last_name','location','profession',
       'current_job','about_me','introduction','search_vector','date_joined',
        )}),
        ('Uploads', {'fields': ('image',)}),
        ('Links', {'fields': ('website','facebook_link','twitter_link','youtube_link',)}),
        ('Permissions', {'fields': ('is_admin','is_active',"is_superuser", 'delete_account','groups', 'user_permissions')}),
                 )
    add_fieldsets = (
        (None, {
                'classes': ('wide',),
                'fields': ('email', 'first_name','username','password1', 'password2')}
            ),
        )
    search_fields = ('email','username')
    ordering = ('first_name',)
    filter_horizontal = ()




admin.site.register(Profile, ProfileAdmin)
admin.site.register(Network)
admin.site.register(NetworkRequest)
admin.site.register(Notification)
admin.site.register(UserRecommendation)
