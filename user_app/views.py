from django.shortcuts import render, get_object_or_404, redirect, get_list_or_404,redirect
from django.views import generic
from django.contrib.auth import authenticate,login
from .models import Profile, Network, NetworkRequest,UserRecommendation,Notification
from django.http import HttpResponse, Http404, JsonResponse
from django.views.generic import View, ListView
from .forms import ProfileRegistrationForm, ProfileSearchForm,LoginForm,ProfileEditForm, QuickSearchForm, EmailForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import logout, login
from django.db.models import Q, F
from django.contrib import messages
from job_app.models import Job
from django.core.files import File
from operator import attrgetter
from django.core.urlresolvers import reverse
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank
from django.contrib import messages
from django.core.mail import EmailMessage, send_mail
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from user_app.tokens import account_activation_token
from PIL import Image
import base64
import pytz
import datetime
import json
import os


# Create your views here.
class Login(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request,'users/home.html', {'view_user': request.user})
        else:
            return render(request, 'index/welcome_page.html', {'login_form' : LoginForm ,'sign_up_form': ProfileRegistrationForm})
    def post(self, request):
        email = request.POST.get('email')
        password = request.POST.get('password')
        user=authenticate(email=email, password=password)
        if user is not None and user.is_active:
            login(request, user)
            return redirect('/')
        messages.error(request, "Invalid/Not Active")
        return redirect('/')

def signup(request):
    form = ProfileRegistrationForm(request.POST)
    if form.is_valid():
        if request.POST.get('add_field'):
		return redirect('/')
	else:
	    user = form.save(commit=False)
            password = form.cleaned_data['password']
            user.set_password(password)
            user.is_active = True
            user.save()
            #current_site = get_current_site(request)
            #subject = 'Activate your account.'
            #message = render_to_string('index/acc_activate_email.html', {
            #'user': user, 'domain': current_site.domain,
            #'uid': urlsafe_base64_encode(force_bytes(user.id)),
            #'token': account_activation_token.make_token(user),
            #})
            #toemail = form.cleaned_data.get('email')
            #email = EmailMessage(subject, message, to =[toemail])
            #email.send()
            #return redirect(reverse('signup_confirm_message'))
	    login(request,user)
	    return redirect('/')
    return render(request, 'index/welcome_page.html', {'sign_up_form': form, 'login_form' : LoginForm })

def signup_confirm_message(request):
    return render(request, 'index/sign_up_confirm.html')

class ActivateLinkView(View):
    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = Profile.objects.get(id =uid)
        except(TypeError, ValueError, OverflowError, Profile.DoesNotExist):
            user = None

        if user is not None and not user.is_active and account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            return render(request, 'others/registration_activation_page.html', {'valid_link': 'True'})
        else:
            return render(request, 'others/registration_activation_page.html', {'user': user, 'invalid_link': 'True', 'form' : EmailForm})

def resend_activate_link(request):
    form = EmailForm(request.POST or None)
    if form.is_valid:
        email = request.POST.get('email')
        try:
            user = Profile.objects.get(email = email)
            current_site = get_current_site(request)
            subject = 'Activate your account.'
            message = render_to_string('index/acc_activate_email.html', {
            'user': user, 'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.id)),
            'token': account_activation_token.make_token(user),
            })
            toemail = email
            email = EmailMessage(subject, message, to =[toemail])
            email.send()
        except(Profile.DoesNotExist):
            user = None
    return HttpResponse('Check your email for the verification code')


@login_required(login_url='/')
def about_me(request, username):
    user = get_object_or_404(Profile,username = username)
    job_post =  user.groups.filter(name= 'Job Posters').exists()
    recommendation = UserRecommendation()
    recommendation_list = recommendation.get_user_recommends(from_user = user)
    return render(request, 'users/about_me.html', {'view_user': user, 'recommendation':recommendation_list, 'job_post':job_post})

@login_required(login_url='/')
def user_log_out(request):
    logout(request)
    return redirect('/')

class EditProfile(LoginRequiredMixin, generic.UpdateView):
    login_url ='/'
    form_class = ProfileEditForm
    template_name = 'users/edit_profile.html'
    success_url ='/'
    def get_object(self, *args, **kwargs):
        user = get_object_or_404(Profile, username  = self.request.user.username)
        return user
    def form_valid(self, form):
        super(EditProfile, self).form_valid(form)
        user = form.instance
        image = form.cleaned_data.get('image')
        x = form.cleaned_data.get('x')
        y = form.cleaned_data.get('y')
        w = form.cleaned_data.get('width')
        h = form.cleaned_data.get('height')
        if image and (x or y or w or h):
            image = Image.open(user.image)
            cropped_image = image.crop((x, y, w+x, h+y))
            resized_image = cropped_image.resize(( 200,200), Image.ANTIALIAS)
            resized_image.save(user.image.path, quality = 95)
        return redirect(self.get_success_url())




@login_required(login_url='/')
def view_network(request,username):
    user = get_object_or_404(Profile, username = username)
    if request.GET.get('search_people'):
        form = ProfileSearchForm(request.GET)
        if form.is_valid():
            qs = Profile.objects.filter(other_user__in= Network.objects.filter(from_user =user))
            search = form.cleaned_data.get('search_people')
            query = SearchQuery(search)
            network_list = qs.annotate(rank=SearchRank(F('search_vector'), query))\
            .filter(search_vector=query)\
            .order_by('-rank')
    else:
	form = ProfileSearchForm
        current_network = Network()
        network_list =  current_network.all_network(from_user = user)
    paginator = Paginator(network_list, 30)
    page = request.GET.get('page')
    try:
        network = paginator.page(page)
    except PageNotAnInteger:
        network = paginator.page(1)
    except EmptyPage:
        network = paginator.page(paginator.num_pages)
    total = len(network_list)
    return render(request, 'users/view_network.html', {
    'total':total,
    'view_user' : user,
    'people': network,
    'form' : form,

    })



class RecommendedView(LoginRequiredMixin, View):
    login_url = '/'
    def get(self, request, username):
        user = get_object_or_404(Profile, username = username)
        if request.GET.get('search_people'):
            form = ProfileSearchForm(request.GET)
            if form.is_valid():
                qs = Profile.objects.filter(recommends__in= UserRecommendation.objects.filter(to_user =user))
                q = form.cleaned_data.get('search_people')
                query = SearchQuery(q)
                recommended_list = qs.annotate(rank=SearchRank(F('search_vector'), query))\
                .filter(search_vector=query)\
                .order_by('-rank')
        else:
	    form  = ProfileSearchForm
            recommendation = UserRecommendation()
            recommended_list =  recommendation.get_user_recommended(to_user = user)
        paginator = Paginator(recommended_list, 30)
        page = request.GET.get('page')
        try:
            recommended = paginator.page(page)
        except PageNotAnInteger:
            recommended = paginator.page(1)
        except EmptyPage:
            recommended = paginator.page(paginator.num_pages)
	total = len(recommended_list)
        return render(request, 'users/view_recommendation.html', {
            'total':total,
            'view_user' : user,
            'people': recommended,
            'form' : form,
         })


@login_required(login_url='/')
def remove_network(request, username):
    to_user = get_object_or_404(Profile, username = username)
    if to_user == request.user:
        raise Http404
    network = get_object_or_404(Network, from_user = request.user , to_user = to_user)
    network = Network()
    network.remove_network(from_user= request.user, to_user = to_user)
    return redirect(reverse('view_user', kwargs={'username':to_user.username}))


@login_required(login_url='/')
def send_request(request, username):
   to_user = get_object_or_404(Profile, username = username)
   if request.user == to_user:
       raise Http404
   try:
       NetworkRequest.objects.get(from_user = to_user , to_user = request.user)
       return accept_request(request =request, username =username)
   except(NetworkRequest.DoesNotExist):
       NetworkRequest.objects.get_or_create(from_user = request.user , to_user = to_user)
       return redirect(reverse('view_user', kwargs={'username':to_user.username}))

@login_required(login_url = '/')
def remove_request(request, username):
    to_user = get_object_or_404(Profile, username = username)
    if request.user == to_user:
        raise Http404
    network_request= get_object_or_404(NetworkRequest, from_user = request.user , to_user = to_user)
    network_request.delete()
    return redirect(reverse('view_user', kwargs={'username':to_user.username}))

@login_required(login_url = '/')
def accept_request(request, username):
    to_user = get_object_or_404(Profile , username= username)
    if request.user == to_user:
        raise Http404
    network_request = get_object_or_404(NetworkRequest, from_user = to_user, to_user = request.user)
    Network.objects.get_or_create(from_user = request.user , to_user = to_user)
    network_request.delete()
    return redirect(reverse('view_user', kwargs={'username':to_user.username}))


@login_required(login_url = '/')
def decline_request(request, username):
    to_user = get_object_or_404(Profile , username= username)
    if request.user == to_user:
        raise Http404
    network_request= get_object_or_404(NetworkRequest,from_user = to_user, to_user = request.user)
    network_request.delete()
    return redirect(reverse('view_user', kwargs={'username':to_user.username}))


@login_required(login_url='/')
def view_user(request, username):
    view_user = get_object_or_404(Profile, username= username)
    if view_user == request.user:
        return redirect('/')
    else:
        template_name='users/home.html'
        page = request.GET.get('page')
        friends =False
        pending = False
        request_received = False
        try:
            Network.objects.get(from_user= request.user, to_user = view_user)
            friends = True
        except(Network.DoesNotExist):
            try:
                NetworkRequest.objects.get(from_user= request.user, to_user = view_user)
                pending = True
            except(NetworkRequest.DoesNotExist):
                try:
                    NetworkRequest.objects.get(from_user= view_user, to_user = request.user)
                    request_received = True
                except(NetworkRequest.DoesNotExist):
                    pass
        try:
            UserRecommendation.objects.get(from_user = request.user, to_user = view_user)
            recommended = True
        except(UserRecommendation.DoesNotExist):
            recommended = False
        return render(request, template_name, {'user' : request.user, 'view_user': view_user, 'friends': friends,
         'pending': pending, 'request_received': request_received,'recommended':recommended} )


def check_username(request):
    username = request.POST.get('username')
    if Profile.objects.all().filter(username__iexact = username).exists():
        data = {'result': 'exist'}
    else :
        data = {'result': 'valid'}
    return HttpResponse(json.dumps(data), content_type = 'application/json')


class DiscoverPeople(LoginRequiredMixin, View):
    login_url ='/'
    template_name = 'users/discover_people.html'
    def get(self, request):
        template_name = 'users/discover_people.html'
        form = ProfileSearchForm
        if request.GET.get('search_people'):
            form = ProfileSearchForm(request.GET)
            if form.is_valid:
                search = request.GET.get('search_people')
                people_list = Profile.objects.annotate(
                search=SearchVector('first_name', 'last_name'),
                ).filter(search = search)
                paginator = Paginator(people_list, 10)
                page = request.GET.get('page')
                try:
                    people = paginator.page(page)
                except PageNotAnInteger:
                    people = paginator.page(1)
                except EmptyPage:
                    people = paginator.page(paginator.num_pages)
                total = people_list.count()
            return render(request, template_name, {'people': people, 'form': form, 'total':total})
        return render(request, template_name, {'form': form})


@login_required
def recommend_user(request, username):
    to_user = get_object_or_404(Profile, username = username)
    if request.user == to_user :
        raise Http404
    else:
        if request.user.recommends.count() < 20 :
            try:
                UserRecommendation.objects.get(from_user = request.user, to_user = to_user).delete()
            except(UserRecommendation.DoesNotExist):
                UserRecommendation.objects.create(from_user = request.user, to_user = to_user)
        else:
            messages.error(request, 'You cant recommend more than 20 people')
        return redirect(reverse('view_user', kwargs={'username':to_user.username}))


class NotificationView(LoginRequiredMixin, View):
    login_url = '/'
    def get(self, request):
        notifications = Notification.objects.filter(to_user = request.user, unread=True).order_by('-created_on')
        return render(request, 'ajax/ajax-notifications.html', {'notifications': notifications})
    def post(self, request):
        id = request.POST.get('id')
        notification = get_object_or_404(Notification, id = id)
        notification.unread = False
        notification.save(update_fields=['unread'])
        notification_count = Notification.objects.filter(to_user = request.user, unread=True).count()
        data ={'result':'success', 'notification_count':notification_count}
        return HttpResponse(json.dumps(data), content_type='application/json')

@login_required(login_url = '/')
def quick_search(request):
    form = QuickSearchForm(request.GET)
    if form.is_valid():
        search = form.cleaned_data.get('quick_search')
        people = Profile.objects.annotate(
            search=SearchVector('first_name', 'last_name'),
            ).filter(search = search)
        if people:
            return render(request, 'ajax/find_people.html', {'people':people})
    data = {'result' : 'null'}
    return HttpResponse(json.dumps(data), content_type= 'application/json')

@login_required(login_url = '/')
def poll_updates(request):
    notification_count= request.user.count_notifications()
    recommendation_count = request.user.count_recommended()
    network_count = request.user.count_network()
    data ={
    'notification_count': notification_count,
    'recommendation_count': recommendation_count,
    'network_count': network_count,
    }
    return HttpResponse(json.dumps(data), content_type='application/json')

@login_required(login_url='/')
def view_account(request, username):
    view_user = request.user
    job_poster = request.user.groups.filter(name = 'Job Posters').exists()
    return render(request, 'users/account.html', {'view_user': view_user, 'job_poster': job_poster})

@login_required(login_url='/')
def delete_account(request, username):
    view_user = request.user
    view_user.is_active = False
    view_user.delete_account = True
    view_user.save()
    logout(request)
    return redirect('/')
def help(request):
    return render(request, 'others/help.html')
def terms_and_condition(request):
    return render(request, 'others/terms.html')
def cookies(request):
    return render(request, 'others/cookies.html')
def privacy(request):
    return render(request, 'others/privacy.html')
