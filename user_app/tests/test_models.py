from django.test import TestCase
from user_app.models import Profile, UserRecommendation, Notification, NetworkRequest, Network


class ProfileModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        #used by all test
        cls.samuel = Profile.objects.create(first_name = 'Samuel', last_name= 'Don',
        email = 'sam00@yahoo.com', profession='Footballer', username='samdon',
        password = 'sam1234'
        )
        # cls.ron =Profile.objects.create(first_name = 'Ron', last_name= 'Will',
        # email = 'ron@yahoo.com', profession='Basketballer', username='ron',
        # password = 'ron1234'
        # )

        # UserRecommendation.objects.create(from_user = Profile.objects.get(id=1)
        # , to_user = Profile.objects.get(id=2))

    def test_first_name_label(self):
        # user = Profile.objects.get(id=1)
        field_label = self.samuel._meta.get_field('first_name').verbose_name
        self.assertEqual(field_label, 'first name')

    def test_first_name_max_length(self):
        # user = Profile.objects.get(id=1)
        max_length = self.samuel._meta.get_field('first_name').max_length
        self.assertTrue(len(self.samuel.first_name) <= max_length)

    def test_get_absolute_url(self):
        # user = Profile.objects.get(id=1)
        self.assertEqual(self.samuel.get_absolute_url(), '/view/{}/'.format(self.samuel.username))

    def test_is_active(self):
        # user = Profile.objects.get(id=1)
        self.assertEqual(self.samuel.is_active, False)

    def test_is_admin(self):
        # user = Profile.objects.get(id=1)
        self.assertEqual(self.samuel.is_admin, False)

    def test_full_name(self):
        # user = Profile.objects.get(id=1)
        self.assertEqual(self.samuel.get_full_name(), '{} {}'.format(self.samuel.first_name, self.samuel.last_name))

    def test_str(self):
        # user = Profile.objects.get(id=1)
        self.assertEqual(str(self.samuel), self.samuel.get_full_name())

    def test_get_into(self):
        # user = Profile.objects.get(id=1)
        self.assertEqual(self.samuel.get_introduction(), "Hello I'm {}".format(self.samuel.first_name))

    def test_is_staff(self):
        # user = Profile.objects.get(id=1)
        self.assertEqual(self.samuel.is_staff, False)

    def test_get_website(self):
        if self.samuel.website:
            self.assertEqual(self.samuel.get_website(), self.samuel.website)
        else:
            self.assertEqual(self.samuel.get_website(), '#')
    def test_get_twitter(self):
        self.assertEqual(self.samuel.get_twitter(), '#')
    def test_get_facebook(self):
        self.assertEqual(self.samuel.get_facebook(), '#')
    def test_get_youtube(self):
        self.assertEqual(self.samuel.get_youtube(), '#')
    # def test_get_website(self):
    #     self.assertEqual(self.samuel.get_website(), '-')


class UserRecommendationModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        #used by all test
        cls.john = Profile.objects.create(first_name = 'John', last_name= 'Don',
        email = 'john@yahoo.com', profession='Footballer', username='john',
        password = 'john1234'
        )
        cls.ronald =Profile.objects.create(first_name = 'Ronald', last_name= 'Will',
        email = 'ronald@yahoo.com', profession='Basketballer', username='ronald',
        password = 'ronald1234'
        )
        UserRecommendation.objects.create(from_user = cls.john
        , to_user = cls.ronald)

    def test_str(self):
        recommendation = UserRecommendation.objects.get(id=1)
        self.assertEqual(str(recommendation), '{} recommends {}'.format(self.john, self.ronald)  )

    # def test_recommendation_save(self):
    #     notification=  Notification.objects.get(id=1)
    #     # user = Profile.objects.get(id=3)
    #     self.assertEqual(Notification.objects.filter(from_user = self.john).exists(), True)

    def test_get_user_recommends(self):
        recommendation = UserRecommendation()
        # user = Profile.objects.get(id=3)
        recommendation_list = recommendation.get_user_recommends(from_user = self.john)
        self.assertEqual( recommendation_list, [self.ronald])

    def test_get_user_recommended(self):
        recommendation = UserRecommendation()
        # user = Profile.objects.get(id=4)
        recommendation_list = recommendation.get_user_recommended(to_user = self.ronald)
        self.assertEqual( recommendation_list, [self.john])

    def test_get_user_recommends_is_empty(self):
        recommendation = UserRecommendation()
        # user = Profile.objects.get(id=4)
        recommendation_list = recommendation.get_user_recommends(from_user = self.ronald)
        self.assertEqual( recommendation_list, [])

    def test_get_user_recommended_is_empty(self):
        recommendation = UserRecommendation()
        # user = Profile.objects.get(id=3)
        recommendation_list = recommendation.get_user_recommended(to_user = self.john)
        self.assertEqual( recommendation_list, [])



class NetworkRequestModelTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.jen =Profile.objects.create(first_name = 'Jen', last_name= 'Don',
        email = 'jen@yahoo.com', profession='Footballer', username='jen',
        password = 'jen1234'
        )
        cls.ronson = Profile.objects.create(first_name = 'Ronson', last_name= 'Will',
        email = 'ronson@yahoo.com', profession='Basketballer', username='ronson',
        password = 'ronald1234'
        )
        cls.request1 = NetworkRequest.objects.create(from_user = cls.jen
        , to_user = cls.ronson)

    def test_str(self):
        self.assertEqual(str(self.request1), '{}, {} wants to add you to his network'.format(self.ronson, self.jen)  )

    def test_all_request_received(self):
        request = NetworkRequest()
        request_list =request.all_request_received(to_user = self.ronson)
        self.assertEqual(request_list, [self.jen])
    def test_all_request_received_other_user(self):
        request = NetworkRequest()
        request_list =request.all_request_received(to_user = self.jen)
        self.assertEqual(request_list, [])

    def count_request(self):
        request = NetworkRequest()
        count =request.count_request_received(to_user = self.jen)
        self.assertEqual(count, 0)
    def count_request_other_user(self):
        request = NetworkRequest()
        count =request.count_request_received(to_user = self.ronson)
        self.assertEqual(count, 1)






    # def test_decline_request(self):


class NetworkModelTestCase(TestCase):
    @classmethod
    def setUp(self):
        self.jin =Profile.objects.create(first_name = 'Jin', last_name= 'Don',
        email = 'Jin@yahoo.com', profession='Footballer', username='Jin',
        password = 'jen1234'
        )
        self.donald = Profile.objects.create(first_name = 'donald', last_name= 'Will',
        email = 'donald@yahoo.com', profession='Basketballer', username='donald',
        password = 'ronald1234'
        )
        self.network1 = Network.objects.create(from_user = self.jin
        , to_user = self.donald)

    def test_str(self):
        self.assertEqual(str(self.network1),'{} and {}'.format(self.jin,self.donald) )

    def test_all_network(self):
        network = Network()
        network_list = network.all_network(from_user = self.jin)
        self.assertEqual(network_list, [self.donald])
    def test_all_network_other_user(self):
        network = Network()
        network_list = network.all_network(from_user = self.donald)
        self.assertEqual(network_list, [self.jin])

    def test_network_count(self):
        network = Network()
        count = network.network_count(from_user = self.jin)
        self.assertEqual(count, 1)
