from django.test import TestCase, RequestFactory, Client
from user_app import views, models
from user_app.models import *
from user_app.forms import *
from django.core.urlresolvers import reverse



class UserAppViewsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.samuel = Profile.objects.create_user(
        # email, first_name, username, password=None
        first_name = 'Samuel',
        email = 'sam@yahoo.com', username='samdon',
        password = 'sam1234'
        )
        cls.samuel.is_active = True
        cls.samuel.save()

        cls.john = Profile.objects.create_user(
        # email, first_name, username, password=None
        first_name = 'John',
        email = 'john@yahoo.com', username='John',
        password = 'john1234'
        )
        cls.john.is_active = True
        cls.john.save()
        cls.client =  Client()

    # 'http://testserver/
    # fixtures = ['data.json']
            # self.assertJSONEqual(
            #     str(responseonse.content, encoding='utf8'),
            #     {'status': 'success'}
            # )
    def test_login(self):
        #test authenticated
        self.client.login(email = 'sam@yahoo.com', password= 'sam1234')
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users/home.html')
        # test not authenticated
        self.client.logout()
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index/welcome_page.html')
        self.assertTrue('login_form' and 'sign_up_form' in response.context)
        # test valid login post
        self.client.logout()
        response = self.client.post('/', {'email': self.samuel.email, 'password': self.samuel.password})
        self.assertEqual(response.status_code, 302)
        # self.assertTrue(self.samuel.is_active)
        #test wrong login credentials
        self.client.logout()
        response = self.client.post('/', {'email': 'wronegemail@yahoo.com', 'password':'wrongpassword@yahoo.com'})
        self.assertEqual(response.status_code, 302)
        # message = list(response.context.get('messages'))[0]
        # self.assertEqual(message, "error")

    def test_sign_up(self):
        #test valid form
        response = self.client.post('/signup/', {'email': 'good12@yahoo.com', 'password': 'go1234',
        'password2': 'go1234','first_name': 'yonda', 'last_name': 'good', 'username':'yonda12', 'profession':'architect'
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index/sign_up_confirm.html')
        #test empty form
        response = self.client.post('/signup/', {})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index/welcome_page.html')
        #test email already exist
        response = self.client.post('/signup/', {'email': self.samuel.email, 'password': 'go1234',
        'password2': 'go1234','first_name': 'yonda', 'last_name': 'good', 'username':'yonda12', 'profession':'architect'
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index/welcome_page.html')
        #test username already exist
        response = self.client.post('/signup/', {'email': 'yah@yahoo.com', 'password': 'go1234',
        'password2': 'go1234','first_name': 'yonda', 'last_name': 'good', 'username':self.samuel.username, 'profession':'architect'
        })
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index/welcome_page.html')






    # def test_edit_profile(self):
    #
    #     self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
    #     response = self.client.post(reverse('profile_edit'),{'first_name': 'don'})
    #     # self.assertEqual(response.status_code, 302)
    #     self.assertTemplateUsed(response, 'users/edit_profile.html')
    #     # self.assertEqual(self.ssamuel.first_name, 'don')


    def test_about_me(self):
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('about_me', kwargs={'username': self.samuel.username}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('view_user' and 'recommendation' in response.context)
    def test_log_out(self):
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('logout'), follow= True)
        self.assertEqual(response.status_code, 200)

    def test_log_out_login_required(self):
        response = self.client.get(reverse('logout'), follow= True)
        self.assertEqual(response.status_code, 200)

    def test_view_network(self):
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('view_network', kwargs={'username': self.samuel.username}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('view_user' and 'people' and 'custom_page_range' and 'total' and 'form' in response.context)
        # test search
        response = self.client.get(reverse('view_network', kwargs={'username': self.samuel.username}), {'search_people': 'john'})
        self.assertEqual(response.status_code, 200)

    def test_remove_network(self):
        #remove network that does not exist
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('remove_network', kwargs={'username': self.john.username}))
        self.assertEqual(response.status_code, 404)
        #remove profile that does not exists
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('remove_network', kwargs={'username': 'jame'}))
        self.assertEqual(response.status_code, 404)
        #remove network
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user1 = Profile.objects.get(username = self.john.username )
        user2 = Profile.objects.get(username = self.samuel.username )
        Network.objects.create(from_user = user1, to_user = user2)
        response = self.client.get(reverse('remove_network', kwargs={'username': self.john.username}))
        self.assertEqual(response.status_code, 302)
    def test_remove_request(self):
        #remove request that does not exist
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('delete_request', kwargs={'username': self.john.username}))
        self.assertEqual(response.status_code, 404)
        #remove profile that does not exists
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('delete_request', kwargs={'username': 'jame'}))
        self.assertEqual(response.status_code, 404)
        #remove request
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user1 = Profile.objects.get(username = self.samuel.username )
        user2 = Profile.objects.get(username = self.john.username )
        NetworkRequest.objects.create(from_user = user1, to_user = user2)
        response = self.client.get(reverse('delete_request', kwargs={'username': self.john.username}))
        self.assertEqual(response.status_code, 302)
    def test_accept_request(self):
        #accept request that does not exist
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('accept_request', kwargs={'username': self.john.username}))
        self.assertEqual(response.status_code, 404)
        #accept profile that does not exists
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('accept_request', kwargs={'username': 'jame'}))
        self.assertEqual(response.status_code, 404)
        #accept request
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user1 = Profile.objects.get(username = self.john.username )
        user2 = Profile.objects.get(username = self.samuel.username )
        NetworkRequest.objects.create(from_user = user1, to_user = user2)
        response = self.client.get(reverse('accept_request', kwargs={'username': self.john.username}))
        self.assertEqual(response.status_code, 302)
    def test_decline_request(self):
        #decline request that does not exist
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('decline_request', kwargs={'username': self.john.username}))
        self.assertEqual(response.status_code, 404)
        #decline profile that does not exists
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('decline_request', kwargs={'username': 'jame'}))
        self.assertEqual(response.status_code, 404)
        #decline request
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user1 = Profile.objects.get(username = self.john.username )
        user2 = Profile.objects.get(username = self.samuel.username )
        NetworkRequest.objects.create(from_user = user1, to_user = user2)
        response = self.client.get(reverse('decline_request', kwargs={'username': self.john.username}))
        self.assertEqual(response.status_code, 302)
    def test_send_request(self):
        #request profile that does not exists
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('send_request', kwargs={'username': 'jame'}))
        self.assertEqual(response.status_code, 404)
        #send request
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user1 = Profile.objects.get(username = self.john.username )
        user2 = Profile.objects.get(username = self.samuel.username )
        # NetworkRequest.objects.create(from_user = user1, to_user = user2)
        response = self.client.get(reverse('send_request', kwargs={'username': self.john.username}))
        self.assertEqual(response.status_code, 302)


    def test_recommended_view(self):
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('view_recommended', kwargs={'username': self.samuel.username}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('view_user' and 'people' and 'custom_page_range' and 'total' and 'form' in response.context)
        # test search
        response = self.client.get(reverse('view_recommended', kwargs={'username': self.samuel.username}), {'search_people': 'john'})
        self.assertEqual(response.status_code, 200)

    def test_view_current_user(self):
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('view_user', kwargs={'username': self.samuel.username}), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_view_other_user(self):
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('view_user', kwargs={'username': self.john.username}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('view_user' and 'friends' and 'request_received' and 'pending' and 'recommended' in response.context)

    def test_check_username_exist(self):
        response = self.client.post(reverse('check_username'), {'username': self.john.username})
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'result': 'exist'}
        )
    def test_check_username_does_not_exist(self):
        response = self.client.post(reverse('check_username'), {'username': 'apple'})
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'result': 'valid'}
        )


    def test_discover_people(self):
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('discover_people'))
        self.assertEqual(response.status_code, 200)
        #test_search
        esponse = self.client.get(reverse('discover_people'), {'search_people': 'john'})
        self.assertEqual(response.status_code, 200)

    def test_discover_people_login_required(self):
        response = self.client.get(reverse('discover_people'))
        self.assertEqual(response.status_code, 302)

    def test_notification_view(self):
        #test get notifications
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('notifications'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'ajax/ajax-notifications.html')
        self.assertTrue('notifications' in response.context)
        # notification that does not extist
        response = self.client.post(reverse('notifications'), {'id':1})
        self.assertEqual(response.status_code, 404)
        # notification that exists
        user1 = Profile.objects.get(username = self.john.username )
        user2 = Profile.objects.get(username = self.samuel.username )
        notification = Notification.objects.create(from_user= user1, to_user = user2, message='hey', category='somecategory')
        response = self.client.post(reverse('notifications'), {'id':notification.id})
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'result': 'success', 'notification_count': 0}
        )
    def test_quick_search(self):
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('quick_search'), {'quick_search': self.samuel.first_name})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'ajax/find_people.html')
        self.assertTrue('people' in response.context)
        #test for no result
        response = self.client.get(reverse('quick_search'), {'quick_search': 'jen'})
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'result': 'null'}
        )
    def test_poll_updates(self):
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('poll_updates'))
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'notification_count': 0, 'recommendation_count': 0}
        )

    def test_recommend_action(self):
        # recommend_action
        #recommend profile that does not exists
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('recommend_action', kwargs={'username': 'jame'}))
        self.assertEqual(response.status_code, 404)
        #recommend profile
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user1 = Profile.objects.get(username = self.john.username )
        user2 = Profile.objects.get(username = self.samuel.username )
        # UserRecommendation.objects.create(from_user = user1, to_user = user2)
        response = self.client.get(reverse('recommend_action', kwargs={'username': self.john.username}))
        self.assertEqual(response.status_code, 302)
