from django.test import TestCase, RequestFactory, Client
from user_app import views, models
from user_app.models import *
from user_app.forms import *
from django.core.urlresolvers import reverse

class UserAppFormTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.samuel = Profile.objects.create_user(
        # email, first_name, username, password=None
        first_name = 'Samuel',
        email = 'sam@yahoo.com', username='samdon',
        password = 'sam1234'
        )
        cls.samuel.is_active = True
        cls.samuel.save()

    def test_register_form(self):
        form = ProfileRegistrationForm({
        'first_name':'Jim', 'last_name': 'Don',
        'email':'jim@yahoo.com', 'profession':'footballer', 'username':'jim',
        'password': 'jim1234','password2': 'jim1234'
        })
        self.assertTrue(form.is_valid())
        #test email exist
        form = ProfileRegistrationForm({
        'first_name':'Jim', 'last_name': 'Don',
        'email':'sam@yahoo.com', 'profession':'footballer', 'username':'jim',
        'password': 'jim1234','password2': 'jim1234'
        })

        self.assertFalse(form.is_valid())
        #test username exist
        form = ProfileRegistrationForm({
        'first_name':'Jim', 'last_name': 'Don',
        'email':'john@yahoo.com', 'profession':'footballer', 'username':'Samdon',
        'password': 'jim1234','password2': 'jim1234'
        })
        self.assertFalse(form.is_valid())
        #test passwords dont matcch
        form = ProfileRegistrationForm({
        'first_name':'Jim', 'last_name': 'Don',
        'email':'john@yahoo.com', 'profession':'footballer', 'username':'samdon',
        'password': 'jim1234','password2': 'jsim1234'
        })
        self.assertFalse(form.is_valid())
        #no username
        form = ProfileRegistrationForm({
        'first_name':'Jim', 'last_name': 'Don',
        'email':'john@yahoo.com', 'profession':'footballer', 'username':'',
        'password': 'jim1234','password2': 'jim1234'
        })
        self.assertFalse(form.is_valid())
        #wrong username pattern
        form = ProfileRegistrationForm({
        'first_name':'Jim', 'last_name': 'Don',
        'email':'john@yahoo.com', 'profession':'footballer', 'username':'123_()',
        'password': 'jim1234','password2': 'jim1234'
        })
        self.assertFalse(form.is_valid())
        #underscore perimtted in username pattern
        form = ProfileRegistrationForm({
        'first_name':'Jim', 'last_name': 'Don',
        'email':'john@yahoo.com', 'profession':'footballer', 'username':'123_',
        'password': 'jim1234','password2': 'jim1234'
        })
        self.assertTrue(form.is_valid())
        
