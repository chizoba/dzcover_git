from django.forms import ModelForm
from django.contrib.auth.models import User
from .models import Profile, Network

from django import forms
from django.db import models
from django.conf.global_settings import LANGUAGES
from django.shortcuts import get_object_or_404
from string import Template
from django.conf import settings
from django.core.validators import RegexValidator
from django.utils.safestring import mark_safe

class ProfileRegistrationForm(ModelForm):
    username_validator = RegexValidator(r'^[0-9a-zA-Z_]{2,}$', 'Only alphanumeric characters are allowed.')
    first_name= forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'First name'}))
    last_name= forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Last name'}))
    email= forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Email'}))
    password= forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
    #password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Confirm password'}), label='Confirm Password')
    #profession= forms.CharField(widget=forms.TextInput(attrs={'placeholder': ' Profession or Field of Study'}), label='Profession or Field of Study', required = False)
    username= forms.CharField(widget=forms.TextInput(attrs={'placeholder': ' Username', 'pattern':'[A-Za-z0-9_]{2,}'}), validators=[username_validator])

    class Meta:
        model = Profile
        fields = ['first_name', 'last_name','username','email', 'password']
    #def clean_password2(self):
     #   password1 = self.cleaned_data.get('password')
      #  password2 = self.cleaned_data.get('password2')
       # if password1 and password2 and password1 != password2:
        #    raise forms.ValidationError('passwords dont match')
        #return password2

    def clean_username(self):
        username = self.cleaned_data.get('username')
        try:
             Profile.objects.get(username__iexact = username)
             raise forms.ValidationError('username exist')
        except Profile.DoesNotExist:
            pass
        return username

class LoginForm(ModelForm):
    email= forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Email', 'class':'form-control'}))
    password= forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class':'form-control'}))
    remember_me = forms.BooleanField(required= False)
    class Meta:
        model= Profile
        fields = ['email','password', 'remember_me']

class ProfileSearchForm(forms.Form):
    search_people = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Search', 'class':'form-control'}), label='')

class QuickSearchForm(forms.Form):
    quick_search = forms.CharField(widget = forms.TextInput(attrs={'placeholder': 'Search People'}), label='', required=False)


class ProfileEditForm(forms.ModelForm):
    image = forms.ImageField(label='image',required=False, error_messages = {'invalid':"Image files only"}, widget=forms.FileInput)
    remove_photo = forms.BooleanField(required=False, label=' remove image?')
    x = forms.FloatField(widget=forms.HiddenInput(),required=False)
    y = forms.FloatField(widget=forms.HiddenInput(),required=False)
    width = forms.FloatField(widget=forms.HiddenInput(),required=False)
    height = forms.FloatField(widget=forms.HiddenInput(),required=False)
    about_me = forms.CharField(max_length=5000, required=False, widget = forms.Textarea(attrs={'rows':'20', 'placeholder':'add your bio, skills, interest...'}))
    introduction = forms.CharField(max_length=150, widget = forms.Textarea(attrs={'rows':'7'}), required=False)

    class Meta:
        model = Profile
        fields = ('first_name', 'last_name','email','profession','location','current_job', 'image', 'twitter_link', 'youtube_link',
        'facebook_link', 'website', 'introduction','x', 'y', 'width', 'height', 'about_me')
	labels = { 'profession' : 'Profession/Field of study' }

class EmailForm(forms.Form):
    email= forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Enter your email'}))
