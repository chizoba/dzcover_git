from .forms import QuickSearchForm
from django.conf import settings
from .models import Notification

def add_variable_to_context(request):
    if request.user.is_authenticated:
        notification_count = request.user.count_notifications()
    else:
        notification_count = ''

    return {
        'quicksearchform': QuickSearchForm,
        'notification_count': notification_count,
        'SITE_URL': settings.SITE_URL
    }
