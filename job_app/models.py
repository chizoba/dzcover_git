from django.db import models
from django.contrib.auth.models import AbstractUser
from user_app.models import Profile
from django.utils import timezone
from django.contrib.postgres.aggregates import StringAgg
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVectorField, SearchVector
from django.db.models.signals import post_save, m2m_changed
from django.template.defaultfilters import slugify
from django.dispatch import receiver
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from itertools import chain
from operator import attrgetter
from django.contrib.sites.models import Site



# Create your models here.


class JobManager(models.Manager):
    def with_documents(self):
        vector = SearchVector('company_name', weight='A') + \
                 SearchVector('profession', weight='B') + \
                 SearchVector('description', weight='D') + \
                 SearchVector('city', weight='D') + \
                 SearchVector('position', weight='C')

        return self.get_queryset().annotate(document=vector)


class Job(models.Model):
    user = models.ForeignKey(Profile, related_name='posted_jobs')
    company_name = models.CharField(max_length= 100)
    profession = models.CharField(max_length= 150)
    about_company = models.TextField()
    position= models.CharField(max_length= 150)
    description = models.TextField()
    qualification = models.TextField()
    more_info = models.TextField( blank = True, null= True)
    city = models.CharField(max_length = 200)
    link = models.URLField(blank = True, null= True)
    created_on = models.DateTimeField(default= timezone.now)
    expires_on = models.DateField()
    job_type = models.CharField(max_length= 50)
    slug = models.SlugField(max_length = 260)
    search_vector = SearchVectorField(null=True, blank= True)
    objects = JobManager()

    class Meta:
        indexes = [
            GinIndex(fields=['search_vector'])
        ]
        ordering = (('-created_on'),)

    def __str__(self):
        return '{}- {}'.format(self.profession, self.position)

    def save(self, *args, **kwargs):
        self.slug = '-'.join((slugify(self.company_name), slugify(self.position))) # set the slug explicitly
        super(Job, self).save(*args, **kwargs)
        if 'update_fields' not in kwargs or 'search_vector' not in kwargs['update_fields'] :
            instance = self._meta.default_manager.with_documents().get(pk=self.pk)
            instance.search_vector = instance.document
            instance.save(update_fields=['search_vector'])

    def get_location(self):
        return '{}.'.format(self.city)

    def get_absolute_url(self):
        return reverse('job:view_job', kwargs = {'job_id': self.id, 'slug': self.slug})

    def get_full_absolute_url(self):
        domain=Site.objects.get_current().domain
        return '%s%s' % (domain, self.get_absolute_url())

    def get_jobs_from_network(self, user):
        all_jobs =[]
        if user.network.all():
            for other_user in user.network.all():
                if other_user.to_user.posted_jobs.all():
                    all_jobs = chain(other_user.to_user.posted_jobs.all().order_by('-created_on'), all_jobs)
        all_jobs= sorted( all_jobs, key=attrgetter('created_on'), reverse= True)
        return all_jobs
