from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, Http404
from django.core.urlresolvers import reverse
from .forms import JobPostForm, JobEditForm, JobSearchForm
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin
from django.views.generic import View, UpdateView, CreateView
from .models import Job
import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from user_app.models import Profile
from user_app.forms import ProfileSearchForm
from django.utils import timezone
from django.contrib.postgres.search import SearchQuery, SearchRank
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from django.db.models import F
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test

# Create your views here.
class CreateJob(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url ='/job/guide'
    def test_func(self):
        return self.request.user.groups.filter(name= 'Job Posters').exists()
    def get(self,request):
        form = JobPostForm
        return render(request,'job/create_job.html', {'form':form})
    def post(self, request):
        form = JobPostForm(request.POST or None)
        if form.is_valid():
            job = form.save(commit=False)
            job.user = request.user
            expiry_day = datetime.timedelta(days = int(request.POST.get('expiry_day')))
            job.expires_on = (timezone.now() + expiry_day)
            job.save()
            return redirect(reverse('job:posted', kwargs={'username': self.request.user.username}))
        else:
            return render(request, 'job/create_job.html', {'form': form})


@login_required(login_url='/')
def view_posted_jobs(request, username):
    user = get_object_or_404(Profile, username = username)
    job_list = user.posted_jobs.all().order_by('-created_on')
    template_name='job/posted_jobs.html'
    if job_list:
        paginator = Paginator(job_list, 10)
        page = request.GET.get('page')
        try:
            posted_jobs = paginator.page(page)
        except PageNotAnInteger:
            posted_jobs = paginator.page(1)
        except EmptyPage:
            posted_jobs = paginator.page(paginator.num_pages)
        total = job_list.count()
        return render(request, template_name, {'view_user' : user, 'posted_jobs': posted_jobs, 'total':total} )
    return render(request, template_name, {'view_user' : user} )


def job_post_guide(request):
    template_name= 'others/job-post-guide.html'
    return render(request, template_name )

@login_required(login_url ='/')
# @user_passes_test()
@user_passes_test(lambda u: u.groups.filter(name= 'Job Posters').exists(), login_url='/job/guide')
def delete_posted_job(request):
    job = get_object_or_404(Job, id = request.GET.get('job_id'))
    if request.user != job.user:
        raise Http404
    else:
        job.delete()
        data = {'result':'success'}
        total = request.user.posted_jobs.count()
        data['job_count'] = 'Jobs: {}'.format(total)
        return HttpResponse(json.dumps(data), content_type='application/json')

@login_required(login_url= '/')
def view_job(request,slug, job_id):
     job = get_object_or_404(Job, id= job_id)
     return render(request, 'job/view_job.html', {'job': job})

class JobEdit(LoginRequiredMixin,UserPassesTestMixin, UpdateView):
    model = Job
    login_url = '/'
    form_class= JobEditForm
    template_name = 'job/job_edit.html'

    def test_func(self):
        return self.request.user.groups.filter(name= 'Job Posters').exists()

    def get_object(self, queryset=None, *args, **kwargs):
        self.job_id = self.kwargs.get('job_id', None)
        return Job.objects.get(id = self.job_id)

    def get_success_url(self):
        return reverse('job:posted', kwargs={'username': self.request.user.username})

@login_required(login_url= '/')
def job_feed(request):
    jobs = Job()
    job_list = jobs.get_jobs_from_network(user= request.user)
    paginator = Paginator(job_list, 10)
    page = request.GET.get('page')
    try:
        job_feed = paginator.page(page)
    except PageNotAnInteger:
        job_feed = paginator.page(1)
    except EmptyPage:
        job_feed = paginator.page(paginator.num_pages)
    total = len(job_list)

    return render(request, 'job/job_feed.html', {'jobs':job_feed,'total':total})


class JobList(View):
    template_name = 'job/discover_jobs.html'
    def get(self, request):
        form = JobSearchForm
        posted_jobs = Job()
        job_feed = posted_jobs.get_jobs_from_network(user= request.user)[:3]
        if request.GET:
            form = JobSearchForm(request.GET)
            if form.is_valid():
                if self.request.GET.get('search') and self.request.GET.get('city'):
                    search = self.request.GET.get('search')
                    city = self.request.GET.get('city')
                    query = SearchQuery(search)
                    job_list = Job.objects.annotate(rank=SearchRank(F('search_vector'), query))\
                    .filter(city = city).filter(search_vector=query)\
                    .order_by('-rank')
                    job_type = self.request.GET.get('job_type')
                    if job_type != 'All':
                        job_list = job_list.filter(job_type__iexact = job_type)

                    paginator = Paginator(job_list, 5) # Show 5 contacts per page
                    page = request.GET.get('page')
                    try:
                        jobs = paginator.page(page)
                    except PageNotAnInteger:
                        # If page is not an integer, deliver first page.
                        jobs = paginator.page(1)
                    except EmptyPage:
                        # If page is out of range (e.g. 9999), deliver last page of results.
                        jobs = paginator.page(paginator.num_pages)

                    total = job_list.count()
                    return render(request, 'job/discover_jobs.html', {'form': form, 'jobs': jobs, total:'total', 'job_feed': job_feed})
        return render(request, 'job/discover_jobs.html', {'form': form, 'job_feed': job_feed})
