from django.core.management.base import BaseCommand, CommandError
from job_app.models import Job
import datetime

class Command(BaseCommand):
    help = 'Delete jobs that have expired'
    def handle(self, *args, **options):
        try:
            Job.objects.filter(expires_on=datetime.datetime.now()).delete()
        except Job.DoesNotExist:
            raise CommandError('Job "%s" does not exist' % Job.company_name)
    #  poll.save()

        self.stdout.write(self.style.SUCCESS('Successfully cleared jobs '))

    # def handle_noargs(self):
    #     Job.objects.filter(expired_on=datetime.datetime.now()).delete()
