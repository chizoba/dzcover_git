from django.conf.urls import url,include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    url(r'^feed/$', views.job_feed, name= 'feed'),
    url(r'^guide/$', views.job_post_guide, name= 'guide'),
    url(r'^posted/(?P<username>\w+)$', views.view_posted_jobs, name= 'posted'),
    url(r'^create/$', views.CreateJob.as_view(), name= 'create'),
    url(r'^delete/$', views.delete_posted_job, name='delete_job'),
    url(r'^edit/(?P<job_id>\w+)/$', views.JobEdit.as_view(), name= 'edit'),
    url(r'^view/(?P<slug>[-\w\d]+),(?P<job_id>\w+)$', views.view_job, name= 'view_job'),
    url(r'^search/$', views.JobList.as_view(), name='discover_jobs'),
]
