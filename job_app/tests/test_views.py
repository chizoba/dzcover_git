from django.test import TestCase, Client, mock, RequestFactory
from job_app.models import *
from job_app.forms import *
from cities_light.models import Region, Country
from job_app.forms import *
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.forms import IntegerField


class JobViewsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.samuel = Profile.objects.create_user(
        first_name = 'Samuel',
        email = 'sam@yahoo.com', username='samdon',
        password = 'sam1234'
        )
        cls.samuel.is_active = True
        cls.samuel.save()
        cls.john = Profile.objects.create_user(
        first_name = 'John',
        email = 'john@yahoo.com', username='John',
        password = 'john1234'
        )
        cls.country = Country.objects.create(name='test')

        cls.john.is_active = True
        cls.john.save()
        cls.client =  Client()

        cls.client = Client()

    def test_view_job(self):
        self.client.login(email = 'sam@yahoo.com', password= 'sam1234')
        region = Region.objects.create(name= 'test', country=self.country)
        user = Profile.objects.get(username = self.samuel.username )
        job = Job.objects.create(
        company_name ='apple', profession='test', position='test',
        description='test', qualification = 'test', state_and_country= region,
        city='test', link='test', expires_on= timezone.now(), job_type='test', user =user,
        )
        response = self.client.get(reverse('job:view_job', kwargs={'job_id': job.id}))
        self.assertEqual(response.status_code, 200)
        #test job that does not exist
        response = self.client.get(reverse('job:view_job', kwargs={'job_id': 100}))
        self.assertEqual(response.status_code, 404)

    def test_delete_note(self):
        #job that does not exists
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('job:delete_job'), {'job_id': 3})
        self.assertEqual(response.status_code, 404)
        #note user and profile user dont match
        region = Region.objects.create(name= 'test', country=self.country)
        user = Profile.objects.get(username = self.john.username )
        job1 = Job.objects.create(
        company_name ='apple', profession='test', position='test',
        description='test', qualification = 'test', state_and_country= region,
        city='test', link='test', expires_on= timezone.now(), job_type='test', user =user,
        )
        response = self.client.get(reverse('job:delete_job'), {'job_id': job1.id})
        self.assertEqual(response.status_code, 404)
        #succesful delete
        # region = Region.objects.create(name= 'test', country=self.country)
        user = Profile.objects.get(username = self.samuel.username )
        job3 = Job.objects.create(
        company_name ='apple', profession='test', position='test',
        description='test', qualification = 'test', state_and_country= region,
        city='test', link='test', expires_on= timezone.now(), job_type='test', user =user,
        )
        response = self.client.get(reverse('job:delete_job'), {'job_id': job3.id})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'result': 'success', 'job_count': 'Jobs: 0'}
        )


    def test_posted_jobs(self):
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        #test jobs availaible
        region = Region.objects.create(name= 'test', country=self.country)
        user = Profile.objects.get(username = self.samuel.username )
        job1 = Job.objects.create(
        company_name ='apple', profession='test', position='test',
        description='test', qualification = 'test', state_and_country= region,
        city='test', link='test', expires_on= timezone.now(), job_type='test', user =user,
        )
        response = self.client.get(reverse('job:posted', kwargs= {'username': self.samuel.username}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('posted_jobs' in response.context)
        #test empty jobs
        response = self.client.get(reverse('job:posted', kwargs= {'username': self.john.username}))
        self.assertEqual(response.status_code, 200)
        self.assertFalse('posted_jobs' in response.context)


    def test_job_feed(self):
        #login user
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('job:feed'))
        self.assertEqual(response.status_code, 200)
        #user not logged in
        self.client.logout()
        response = self.client.get(reverse('job:feed'))
        self.assertEqual(response.status_code, 302)

    def test_create_job(self):
        #get create form
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        response = self.client.get(reverse('job:create'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('form' in response.context)
        self.assertTemplateUsed(response, 'job/create_job.html')
        #post in create form
        # region = Region.objects.create(name= 'test', country=self.country)
        # response = self.client.post(reverse('job:create'), {
        # "company_name" :'apple', "profession":'test', "position":'test',
        # "description":'test', "qualification" :'test', "state_and_country": region,
        # "city":'test', "link":'test', "expiry_day": 20, "job_type":'Full-time'
        # })
        # self.assertEqual(response.status_code, 302)
    def test_fields(self):
        form = JobPostForm({})
        self.assertEqual(11, len(form.fields))
        self.assertTrue("company_name" in form.fields.keys())
        self.assertTrue("profession" in form.fields.keys())
        self.assertTrue("position" in form.fields.keys())
        self.assertTrue("link" in form.fields.keys())
        self.assertTrue("description" in form.fields.keys())
        self.assertTrue("qualification" in form.fields.keys())
        self.assertTrue("job_type" in form.fields.keys())

        self.assertTrue("city" in form.fields.keys())
        self.assertTrue("more_info" in form.fields.keys())
        self.assertTrue("state_and_country" in form.fields.keys())
        self.assertTrue("expiry_day" in form.fields.keys())
        # self.assertTrue("user" in form.fields.keys())
        # self.assertTrue("expires_on" in form.fields.keys())
        self.assertIsInstance(form.fields['expiry_day'], IntegerField)
        # self.assertEqual(2, form.fields['height'].max_value)

    # def test_valid_data(self):
    #     region = Region.objects.create(name= 'test', country=self.country)
    #     form = JobPostForm({"company_name" :'apple', "profession":'test', "position":'test',
    #         "description":'test', "qualification" :'test', "state_and_country": region,
    #         "city":'test', "expiry_day": 20, "job_type":'Full-time',
    #         "link":'https://lukewickstead.wordpress.com/2015/07/16/django-testing/#TestingForms', "more_info":'Full',
    #
    #         })

        self.assertTrue(form.is_valid())

    # def test_job_form(self):
    #     user = Profile.objects.get(username = self.samuel.username )
    #     region = Region.objects.create(name= 'test', country=self.country)
    #
    #     form = JobPostForm(data={
    #     "company_name" :'apple', "profession":'test', "position":'test',
    #     "description":'test', "qualification" :'test', "state_and_country": region,
    #     "city":'test', "expiry_day": '20', "job_type":'Full-time'
    #     })
    #     self.assertTrue(form.is_valid())

    def test_discover_job(self):
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        region = Region.objects.create(name= 'test', country=self.country)
        response = self.client.get(reverse('job:discover_jobs'), {'state_and_country': region, 'search': 'Software', 'job_type': 'All'})
        self.assertEqual(response.status_code, 200)
