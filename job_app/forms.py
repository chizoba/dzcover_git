from django.forms import ModelForm
from django import forms
from .models import Job
from datetime import date


class JobPostForm(ModelForm):
    job_type_choices = (('Full-time', 'Full-time'),('Part-time', 'Part-time'),('Coop/Intern', 'Coop/Intern'),('Voluntuary', 'Voluntuary'))
    company_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Company name'}))
    profession = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Profession'}))
    position = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Position'}))
    job_type = forms.ChoiceField(widget=forms.Select(), choices = job_type_choices, label = 'Job Type')
    link = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Link'}),  required = False)
    city = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'City', 'autocomplete':'off'}))
    expiry_day = forms.IntegerField(min_value = 7, max_value = 90,widget=forms.NumberInput(attrs={'placeholder': 'number'}))
    about_company = forms.CharField(widget= forms.Textarea(attrs={'rows':'10', 'data-min-rows': '10', 'class':'autoExpand'}),label='About your company', max_length= 1000)
    description = forms.CharField(widget= forms.Textarea(attrs={'rows':'10', 'data-min-rows': '10', 'class':'autoExpand'}),label='Description*', max_length= 5000)
    qualification = forms.CharField(widget= forms.Textarea(attrs={'rows':'10', 'data-min-rows': '10', 'class':'autoExpand'}),label='Qualification', max_length= 5000)
    more_info = forms.CharField(widget= forms.Textarea(attrs={'rows':'5', 'data-min-rows': '5', 'class':'autoExpand','placeholder':'optional'}),label='More Info', max_length= 1500, required= False)
    class Meta:
        model = Job;
        fields = ['company_name', 'profession', 'position','description','about_company',

        'qualification','city','link','job_type', 'more_info']

class JobEditForm(ModelForm):
    job_type_choices = (('Full-time', 'Full-time'),('Part-time', 'Part-time'),('Coop/Intern', 'Coop/Intern'),('Voluntuary', 'Voluntuary'))
    company_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Company name'}))
    profession = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Profession'}))
    position = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Position'}))
    job_type = forms.ChoiceField(widget=forms.Select(), choices = job_type_choices, label = 'Job Type')
    link = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Link', 'autocomplete':'off'}),  required = False)
    city = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'City'}))
    about_company = forms.CharField(widget= forms.Textarea(attrs={'rows':'10', 'data-min-rows': '10', 'class':'autoExpand'}),label='About your company', max_length= 1000)
    description = forms.CharField(widget= forms.Textarea(attrs={'rows':'10', 'data-min-rows': '10', 'class':'autoExpand'}),label='Description*', max_length= 5000)
    qualification = forms.CharField(widget= forms.Textarea(attrs={'rows':'10', 'data-min-rows': '10', 'class':'autoExpand'}),label='Qualification', max_length= 5000)
    more_info = forms.CharField(widget= forms.Textarea(attrs={'rows':'5', 'data-min-rows': '5', 'class':'autoExpand','placeholder':'optional'}),label='More Info', max_length= 1500, required= False)
    class Meta:
        model = Job;
        fields = ['company_name', 'profession', 'position','description','about_company',
        'qualification','city','link','job_type', 'more_info']


class JobSearchForm(ModelForm):
    job_type_choices = (('All', 'All'),('Full-time', 'Full-time'),('Part-time', 'Part-time'),('Coop/Intern', 'Coop/Intern'),('Voluntuary', 'Voluntuary'))
    search= forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Search for Job','class':'form-control'}))
    city = forms.CharField(widget=forms.TextInput(attrs={'autocomplete':'off',}), required = False)
    job_type = forms.ChoiceField(widget = forms.Select(), choices = job_type_choices, label='Type')

    class Meta:
        model = Job;
        fields = ['city', 'job_type']

