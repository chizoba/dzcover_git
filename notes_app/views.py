from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from .models import Note
from .forms import NoteForm
from django.contrib import messages
from django.http import HttpResponse, Http404
from user_app.models import Profile
from django.views.generic import View, UpdateView, CreateView
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db.models import Q, F
from django.http import Http404
from django.contrib.auth.decorators import login_required
import json

class ViewNotes(LoginRequiredMixin,View):
    login_url = '/'
    def get(self, request,username):
        form = NoteForm
        user = get_object_or_404(Profile, username= username)
        note_list = Note.objects.filter(user = user)
        paginator = Paginator(note_list, 10)
        page = request.GET.get('page')
        try:
            notes = paginator.page(page)
        except PageNotAnInteger:
            notes = paginator.page(1)
        except EmptyPage:
            notes = paginator.page(paginator.num_pages)
   	total = note_list.count()
        return render(request, 'note/notes.html', {'view_user': user, 'notes': notes, 'form': form, 'total':total})

class CreateNote(LoginRequiredMixin, CreateView):
    login_url='/'
    model = Note
    form_class = NoteForm
    template_name = 'note/update_note.html'
    def get_success_url(self):
        username = self.request.user.username
        return reverse('notes:view_notes', kwargs={'username':username})
    def form_valid(self, form):
        note = form.save(commit= False)
        note.user = self.request.user
        note.save()
        return redirect(self.get_success_url())

def read_note(request,username,slug,id):
    note = get_object_or_404(Note, id = id )
    response = render(request, 'note/read_note.html', {'note':note})
    if request.user.is_active and request.user != note.user:
        note.views = F('views') + 1
        note.save()
    return response


@login_required(login_url='/')
def delete_note(request,username):
    user = get_object_or_404(Profile,username = username)
    note = get_object_or_404(Note, id = request.GET.get('id'))
    if note.user != request.user :
        raise Http404
    else:
        note.delete()
        count = user.notes.count()
        data = {'result': 'success', 'total_notes': count}
        return HttpResponse(json.dumps(data), content_type ='application/json')



class EditNote(LoginRequiredMixin, UpdateView):
    login_url='/'
    model = Note
    form_class = NoteForm
    template_name = 'note/update_note.html'
    def get_success_url(self):
        username = self.request.user.username
        return reverse('notes:view_notes', kwargs={'username':username})
