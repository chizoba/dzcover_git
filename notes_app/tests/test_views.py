from django.test import TestCase, Client
# from notes_app import views, models
from notes_app.models import *
from notes_app.forms import *
from django.core.urlresolvers import reverse



class NoteAppViewsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.samuel = Profile.objects.create_user(
        # email, first_name, username, password=None
        first_name = 'Samuel',
        email = 'sam@yahoo.com', username='samdon',
        password = 'sam1234'
        )
        cls.samuel.is_active = True
        cls.samuel.save()

        cls.john = Profile.objects.create_user(
        # email, first_name, username, password=None
        first_name = 'John',
        email = 'john@yahoo.com', username='John',
        password = 'john1234'
        )
        cls.john.is_active = True
        cls.john.save()
        cls.client =  Client()

        cls.client = Client()

    
    def test_view_notes(self):
        #for initially getting notes
        self.client.login(email = 'sam@yahoo.com', password= 'sam1234')
        response = self.client.get(reverse('notes:view_notes', kwargs={'username': self.samuel.username}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'note/notes.html')
        self.assertTrue('view_user' and 'notes' and 'form' and 'total' and 'custom_page_range' in response.context )
        response = self.client.post(reverse('notes:view_notes', kwargs={'username': self.samuel.username}), {'title':'Little Man'
        , 'content': 'Give  it all you got', 'tags': 'Believe in God' })
        self.assertEqual(response.status_code, 302)


    def test_read_note(self):
        self.client.login(email = 'sam@yahoo.com', password= 'sam1234')
        user2 = Profile.objects.get(username = self.john.username )
        note = Note.objects.create(title= 'explain', content='little man', tags='some tag', user=user2)
        response = self.client.get(reverse('notes:read_note', kwargs={'username': self.samuel.username, 'id':note.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'note/read_note.html')
        self.assertTrue('note' in response.context)
        # self.assertEqual(note.views, 1)

    def test_delete_note(self):
        #profile that does not exists
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user = Profile.objects.get(username = self.samuel.username )
        note2 = Note.objects.create(title= 'explain', content='little man', tags='some tag', user=user)
        response = self.client.get(reverse('notes:delete_note', kwargs={'username': 'jony'}), {'id': note2.id})
        self.assertEqual(response.status_code, 404)
        #note that does not exists
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user = Profile.objects.get(username = self.samuel.username )
        note2 = Note.objects.create(title= 'explain', content='little man', tags='some tag', user=user)
        response = self.client.get(reverse('notes:delete_note', kwargs={'username': self.samuel.username}), {'id': 5})
        self.assertEqual(response.status_code, 404)
        #note user and profile user dont match
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user = Profile.objects.get(username = self.john.username )
        note = Note.objects.create(title= 'explain', content='little man', tags='some tag', user=user)
        response = self.client.get(reverse('notes:delete_note', kwargs={'username': self.john.username}), {'id': note.id})
        self.assertEqual(response.status_code, 404)
        #succesful delete
        self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
        user = Profile.objects.get(username = self.samuel.username )
        note3 = Note.objects.create(title= 'explain', content='little man', tags='some tag', user=user)
        response = self.client.get(reverse('notes:delete_note', kwargs={'username': self.samuel.username}), {'id': note3.id})
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {'result': 'success', 'total_notes': 2}
        )


    # def test_edit_note(self):
    #     self.client.login(email = 'sam@yahoo.com', password = 'sam1234')
    #     user = Profile.objects.get(username = self.samuel.username )
    #     note = Note.objects.create(title= 'explain', content='little man', tags='some tag', user=user)
    #     response = self.client.post(reverse('notes:edit', kwargs={'pk': note.id, 'username': self.samuel.username}),{'content': 'hey'})
    #     self.assertEqual(response.status_code, 302)
