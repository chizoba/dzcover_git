from django.db import models
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify
from itertools import chain
from operator import attrgetter
from user_app.models import Profile
from django.contrib.sites.models import Site
import datetime
import pytz
# Create your models here.

class Note(models.Model):
    user = models.ForeignKey(Profile, related_name ='notes')
    content = models.TextField(max_length=10000)
    title = models.CharField(max_length=130)
    slug =models.SlugField(max_length = 150)
    created_on = models.DateTimeField(default = timezone.now)
    views = models.IntegerField(default = 0)


    class Meta:
        ordering = ['-created_on']

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title) # set the slug explicitly
        super(Note, self).save(*args, **kwargs) # call Django's save()

    def __str__(self):
        return 'note by {}'.format(self.user)

    def get_absolute_url(self):
        return reverse('notes:read_note', kwargs = {'username': self.user.username, 'id': self.id, 'slug': self.slug})

    def get_full_absolute_url(self):
        domain=Site.objects.get_current().domain
        return '%s%s' % (domain, self.get_absolute_url())

    def count_views(self):
        total = self.views
        total_str = str(total)
        if total >= 10000000:
            return '{}.{}B views'.format(total_str[:2], total_str[2])
        if total >= 1000000000:
            return '{}.{}B views'.format(total_str[0], total_str[1])
        if total >= 100000000:
            return '{}M views'.format(total_str[:3])
        if total >= 10000000:
            return '{}.{}M views'.format(total_str[:2], total_str[2])
        if total >= 1000000:
            return '{}.{}M views'.format(total_str[0], total_str[1])
        if total >= 100000:
            return '{}K views'.format(total_str[:3])
        if total >= 10000:
            return '{}.{}K views'.format(total_str[:2], total_str[2])
        if total == 1:
            return '1 view'
        if total == 0 :
            return ''
        else:
            return '{} views'.format(total_str)
