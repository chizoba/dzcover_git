from .models import Note
from django.forms import ModelForm
from django import forms



class NoteForm(ModelForm):
    content = forms.CharField(max_length = 10000, widget = forms.Textarea(attrs={'rows':'30', 'placeholder':'Start writing..'}), required=True)
    class Meta:
        model= Note
        fields = ['title','content']
