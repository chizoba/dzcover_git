from django.conf.urls import url,include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    url(r'^$', views.ViewNotes.as_view(), name= 'view_notes'),
    url(r'^create$', views.CreateNote.as_view(), name= 'create'),
    url(r'^read/(?P<slug>[-\w\d]+),(?P<id>\w+)$', views.read_note, name= 'read_note'),
    url(r'^edit/(?P<pk>\w+)$', views.EditNote.as_view(), name= 'edit'),
    url(r'^delete/$', views.delete_note, name='delete_note'),
]
