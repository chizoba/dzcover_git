"""workpage URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from user_app import views as user
# from search_app import views as search
from django.contrib.auth import views as auth_views



urlpatterns = [
    url(r'^$', user.Login.as_view(), name= 'index'),
    url(r'^signup/$', user.signup, name='signup'),
    url(r'^signup/confirm$', user.signup_confirm_message, name='signup_confirm_message'),
    url(r'^admin/', admin.site.urls),
    url(r'^register/username/$', user.check_username, name='check_username'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        user.ActivateLinkView.as_view(), name='activate'),
    url(r'^resend/activation/$', user.resend_activate_link, name='resend_activation'),
    url(r'^logout/$', user.user_log_out, name='logout'),
    url(r'^view/(?P<username>\w+)/$', user.view_user, name='view_user'),
    url(r'^view/(?P<username>\w+)/network/$', user.view_network, name='view_network'),
    url(r'^view/(?P<username>\w+)/recommendation/$', user.RecommendedView.as_view(), name='view_recommended'),
    url(r'^network/delete/(?P<username>\w+)/$', user.remove_network, name='remove_network'),
    url(r'^job/', include('job_app.urls', namespace='job')),
    url(r'^people/$', user.DiscoverPeople.as_view(), name='discover_people'),
    url(r'^notifications/$', user.NotificationView.as_view(), name='notifications'),
    url(r'^quicksearch/$', user.quick_search, name='quick_search'),
    url(r'^poll/updates/$' ,user.poll_updates, name='poll_updates'),
    url(r'^request/accept/(?P<username>\w+)/$' ,user.accept_request, name='accept_request'),
    url(r'^request/decline/(?P<username>\w+)/$' ,user.decline_request, name= 'decline_request'),
    url(r'^request/send/(?P<username>\w+)/$' ,user.send_request, name='send_request'),
    url(r'^recommend/(?P<username>\w+)/$' ,user.recommend_user, name='recommend_action'),
    url(r'^request/remove/(?P<username>\w+)/$' ,user.remove_request, name='delete_request'),
    url(r'^edit/profile/$', user.EditProfile.as_view(), name='profile_edit'),
    url(r'^view/(?P<username>\w+)/album/', include('album_app.urls', namespace='album')),
    url(r'^view/(?P<username>\w+)/notes/', include('notes_app.urls', namespace='notes')),
    url(r'^view/(?P<username>\w+)/about-me/', user.about_me, name='about_me'),
    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^about/$', user.help, name='help'),
    url(r'^privacy/$', user.privacy, name='privacy'),
    url(r'^cookies/$', user.cookies, name='cookies'),
    url(r'^view/(?P<username>\w+)/account/$', user.view_account, name='view_account'),
    url(r'^(?P<username>\w+)/account/delete/$', user.delete_account, name='delete_account'),
    url(r'^terms-and-conditions/$', user.terms_and_condition, name='terms')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
