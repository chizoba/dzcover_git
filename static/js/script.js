$(document).ready( function() {



 $('[data-toggle="popover"]').popover();

 $('#id_state_and_country').select2({
   width: '100%'
 }
 );

$('#id_job_type').select2();


$("a[href='none']").click(function(e){
  e.preventDefault()
})

$(".test").click(function(e){
 alert('hey')
})
//TeleportAutocomplete.init('#id_city').on('change', function(value) { console.log(value); });
//new TeleportAutocomplete({ el: '#id_city', maxItems: 5 });
/* SCRIPT TO OPEN THE MODAL WITH THE PREVIEW */
      $("#id_image").change(function () {
        if (!(/\.(gif|jpg|jpeg|png)$/i).test($(this).val())) {
            $('#fileError').html('<p> Upload only images with format gif,jpg,jpeg,png </p>').css('color','red').show()
            $(this).val('')
            $("#image").attr("src", '');
            $image.cropper("destroy");
            }
        else{
          var file_size = this.files[0].size;
          if (file_size <= 5242880 ){
        if (this.files && this.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            $('#fileError').html('')

            $("#image").attr("src", e.target.result);
            var $image = $("#image");
            $image.cropper("destroy");
            $image.cropper({
              viewMode: 1,
              aspectRatio: 1/1,
              minCropBoxWidth: 200,
              minCropBoxHeight: 200,
              cropBoxResizable: false,
              dragCrop: false,
              dragMode: 'move',
              maxContainerWidth: 400,
              maxContainerHeight: 400,
              responsive: true,
              modal:false,
              guides: false,

              ready: function () {
                $image.cropper("setCanvasData", canvasData);
                $image.cropper("setCropBoxData", { width: 200, height: 200 });
              }
            });
          }
          reader.readAsDataURL(this.files[0]);


        }

      }
      else{
        $('#fileError').html('<p> Image is bigger than 5mb </p>').css('color','red').show()
        $(this).val('')
        $("#image").attr("src", '');
        $image.cropper("destroy");
      }
      }
      });

  var $image = $("#image");
  var cropBoxData;
  var canvasData;
  $("#modalCrop").on("hidden.bs.modal", function () {
    cropBoxData = $image.cropper("getCropBoxData");
    canvasData = $image.cropper("getCanvasData");
  });

  // Enable zoom in button
  $(".js-zoom-in").click(function () {
    $image.cropper("zoom", 0.1);

  });

  // Enable zoom out button
  $(".js-zoom-out").click(function () {
    $image.cropper("zoom", -0.1);
  });

  /* SCRIPT TO COLLECT THE DATA AND POST TO THE SERVER */
     $(".js-crop-and-upload").click(function () {
       var cropData = $image.cropper("getData");
       $("#id_x").val(cropData["x"]);
       $("#id_y").val(cropData["y"]);
       $("#id_height").val(cropData["height"]);
       $("#id_width").val(cropData["width"]);
       $("#modalCrop").modal('hide')
     });


function check_updates(){
  $.getJSON('/poll/updates/', function (data) {
    if (data) {
      $('#notification-count').text(data.notification_count)
	$('.recommendation-count').html("<strong>"+data.recommendation_count+"</strong>")
      $('.network-count').html("<strong>"+data.network_count+"</strong>")
    }
    setTimeout(check_updates, 10000);
  });
};

check_updates()

$('#id_photo').change(function(){
  if ($(this).val() != ''){
  if (!(/\.(gif|jpg|jpeg|png)$/i).test($(this).val())) {
    $('#fileError').html('<p> Upload only images with format gif,jpg,jpeg,png </p>').css('color','red').show()
    $(this).val('')
    }
  else{
  var file_size = this.files[0].size;
  if (file_size <= 5242880 ){
      if (this.files && this.files[0]){
        var reader = new FileReader();
        reader.onload = function(e){
          $('#preview_img').attr('src', e.target.result);
          $('#fileError').hide()
        }
        reader.readAsDataURL(this.files[0]);
      }
    }
  else{
    $('#fileError').html('<p> Image is bigger than 5mb </p>').css('color','red').show()
    $(this).val('')
  }
}
}
})


 // insert_urlname
   $('#id_username').keyup(function() {
     var elem = $(this).val()
     if (elem == ''){
       $('.insert_urlname').text('username')
     }
     else{
     $('.insert_urlname').text(elem)
   }
   })

    $('#id_username').keyup(function() {
      var reg=/[^a-zA-Z0-9_]+/
      var username = $('#id_username').val()
      if (username && username.length > 1 ) {
          if (reg.test(username)){
              $('#check_username').html('A valid username can contain only letters, numbers or _ and must be greater than 1 character').css('font-size','12px')
       }
          else{
              $.ajax({
                 type: 'POST',
                 url: '/register/username/',
                 data: {
                    'username' : username,
                     'csrfmiddlewaretoken': $('input[name = csrfmiddlewaretoken]').val()
                 },
                 success: function(data){
                   if (data.result == 'exist'){
			$('#check_username').html('username exist').css('font-size','12px')
                 }
                 else{
                     $('#check_username').html('username valid').css('font-size','12px')
                 }
                 }
             })
            }
   }
   else{
      $('#check_username').html('')
   }
 })

 $('#id_username').blur(function(){
   var username = $('#id_username').val()
   if (username && username.length < 2 ) {
          $('#check_username').html('<i class="fa fa-info-circle"></i> A valid username can contain only letters, numbers or _ and must be greater than 3 characters').css('color', 'red').css('font-size','10px')
 }
 })

$('.delete-photo').click(function(){
  var elem = $(this)
  var card =elem.closest('.photo-card')
  var id = elem.closest('.photo-card').data('photo-id')
  $.ajax({
    type:'GET',
    url: 'delete/' ,
    data: {
      'photo-id': id,
    },
    success: function(data){
      if (data.result == 'success'){
        elem.closest('.photo-card').remove()
        $('#total_photos').text(data.total_photos)
      }
    }
  })

})

  $('.delete-note').click(function(){
  var elem = $(this)
  var card =elem.closest('.note-card')
  var id = card.data('id')
  $.ajax({
    type:'GET',
    url: 'delete/' ,
    data: {
      'id': id,
    },
    success: function(data){
      if (data.result == 'success'){
        elem.closest('.note-card').remove()
        $('#total_notes').text(data.total_notes)
      }
    },
  })

  })


$('.job-card-container').on('click', '.btn-delete-job-post', function(){
  elem = $(this)
  id = $(this).closest('.job-card').data('job-id')
  $.ajax({
      type: "GET",
      url: "/job/delete",
      data: { 'job_id' : id
      },
      success: function(data){
        if (data.result == 'success'){
        elem.closest('.job-card').remove()
        $('#job_count').text(data.job_count)
      }

      }
  });
})


$(document).on('click', '.get-job-link', function(){
  id = $(this).siblings('.show-job-link').toggle()

})

$('.job-card-container').on('click', '.get-link', function(){
  elem = $(this)
  elem.siblings('.show-link').toggle()
})

$('#notification-toggle').click(function(){
  $.ajax({
     type: 'GET',
     url: '/notifications/',
     success: function(data){
           $('#notification-view').html(data)
     }
 })

})

$('#notification-view').on('click', '.notification-remove', function(){
  var card = $(this).closest('.notification-view-card')
  var id = card.data('id')
  $.ajax({
     type: 'POST',
     url: '/notifications/',
     data: {'id': id, 
'csrfmiddlewaretoken': $('input[name = csrfmiddlewaretoken]').val()},
     success: function(data){
        card.remove()
	$('#notification-count').text(data.notification_count)
     }
 })
})

//in use
$('#id_quick_search').keyup(function(){
  search = $(this).val()
  if (search !== ''){
  $.ajax({
     type: 'GET',
     url: '/quicksearch/',
     data: {
         'quick_search' : search,
     },
     success: function(data){
       if (data !== 'null') {
       $('#quicksearch-result').html(data).show()
      }
      else{
        $('#quicksearch-result').html('').hide()
      }
     }

 })
  }
  else{
    $('#quicksearch-result').html('').hide()

  }

})

$('#quicksearch-result').on('click', '.close-search', function(){
  $('#id_quick_search').val('')
  $('#quicksearch-result').html('').hide()

})

$('.quicksearch-form').submit(function(e) {
  e.preventDefault()
})

$(".copy-note-link").click(function(){
        url = $(this).closest('.note-card').data("url")
        $("#link-modal").modal("show");
        $('#note-link').text(url)
      })




});
